#!/usr/bin/env bashio

PASSWORD=$(bashio::config 'password')

echo -e "${PASSWORD}\n${PASSWORD}" | passwd root

bashio::log.info "${PASSWORD}"